CREATE TABLE public.annonce
(
    idannonce integer NOT NULL,
    titre character varying(100) COLLATE pg_catalog."default",
    quantite integer,
    prix double precision,
    prixtotal double precision,
    remise double precision,
    edition character varying(50) COLLATE pg_catalog."default",
    isbn character varying(13) COLLATE pg_catalog."default",
    dateedition date,
    dateannonce date,
    iduser integer NOT NULL,
    ville character varying COLLATE pg_catalog."default",
    CONSTRAINT annonce_pkey PRIMARY KEY (idannonce)
)





   -------------------
   
CREATE TABLE public.archive
(
    idannonce integer,
    titre character varying(100) COLLATE pg_catalog."default",
    quantite integer,
    prix double precision,
    prixtotal double precision,
    remise double precision,
    edition character varying(50) COLLATE pg_catalog."default",
    isbn character varying(13) COLLATE pg_catalog."default",
    dateedition date,
    dateannonce date,
    iduser numeric,
    ville character varying COLLATE pg_catalog."default"
)
    
   -------------
   
CREATE TABLE public.connexion
(
    login character varying COLLATE pg_catalog."default" NOT NULL,
    mdp character varying COLLATE pg_catalog."default" NOT NULL,
    iduser integer,
    CONSTRAINT connexion_pkey PRIMARY KEY (login)
)

    
   -----------------------
   
   
CREATE TABLE public.utilisateur
(
    iduser integer NOT NULL,
    nom character varying COLLATE pg_catalog."default",
    prenom character varying COLLATE pg_catalog."default",
    mail character varying COLLATE pg_catalog."default",
    telephone character varying COLLATE pg_catalog."default",
    nomlibrairie character varying COLLATE pg_catalog."default",
    adresselibrairie character varying COLLATE pg_catalog."default",
    ville character varying COLLATE pg_catalog."default",
    CONSTRAINT utilisateur_pkey PRIMARY KEY (iduser)
)

    
   
   -------------
   
   
CREATE SEQUENCE public.incrementannnonce
    INCREMENT 1
    START 3
    MINVALUE 3
    MAXVALUE 9223372036854775807
    CACHE 1;

   
   CREATE SEQUENCE public.incrementuser
    INCREMENT 10
    START 2000
    MINVALUE 2000
    MAXVALUE 9223372036854775807
    CACHE 1;
   
   
   -----------------
   
   
   
CREATE TRIGGER archiver_annonce
    BEFORE DELETE
    ON public.annonce
    FOR EACH ROW
    EXECUTE PROCEDURE public.archivage();
   
   CREATE FUNCTION public.archivage()
    RETURNS trigger
    LANGUAGE 'plpgsql'
AS $BODY$
declare 
	ida int;
	titrea varchar;
	qtte int;
	prixa float;
	prixt float;
	remisea float;
	editiona varchar;
	dateed date;
	datean date;
	idu int;
	ville varchar;
	rec record;
begin
	for rec in (select * from annonce) loop
		if rec.idannonce = old.idannonce then
			raise notice 'oui';
			insert into archive (
			idannonce ,
			titre ,
			quantite ,
			prix ,
			prixtotal ,
			remise ,
			edition, 
			isbn, 
			dateedition, 
			dateannonce, 
			iduser, 
			ville)
			values(
			old.idannonce,
			old.titre,
			old.quantite,
			old.prix,
			old.prixtotal,
			old.remise,
			old.edition,
			old.isbn,
			old.dateedition,
			old.dateannonce,
			old.iduser,
			old.ville);
		end if;
	end loop;
	
RETURN rec;
END;
$BODY$;

