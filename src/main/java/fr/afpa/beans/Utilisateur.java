package fr.afpa.beans;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Utilisateur {

	private int id;
	private String nom;
	private String prenom;
	private String mail;
	private String telephone;
	private String nomLibrairie;
	private Connexion login;
	private Adresse adresse;
}
