package fr.afpa.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import fr.afpa.beans.Connexion;
import fr.afpa.beans.Utilisateur;
import fr.afpa.control.GeneralControl;

public class DaoLog {

	
	/**
	 * 
	 * @return
	 */
	public ArrayList<Connexion> recupLogs() {
		
		ResultSet listLog = null;
		ArrayList<Connexion> retourLog = new ArrayList<Connexion>();
		
		try {
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/libdata", "libadmin", "libadmin");
			
			String strQuery = "SELECT * FROM connexion";
			
			Statement stServices = conn.createStatement();
			
			listLog = stServices.executeQuery(strQuery);
			
			Connexion login = null;
			
			while (listLog.next()) {
					login = new Connexion();
					login.setLogin(listLog.getString("login"));
					login.setMdp(listLog.getString("mdp"));
					login.setIdUser(listLog.getInt(3));

					retourLog.add(login);
				}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retourLog;
	}
	
	
	/**
	 * 
	 * @param user
	 * @param userTemp
	 */
	public static void insertInto(Utilisateur user, Utilisateur userTemp) {
		
		try {
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/libdata", "libadmin", "libadmin");
			conn.setAutoCommit(false);
			
			String strQuery = "insert into connexion (login ,mdp,iduser)"
					+ "values ( "
					+ "'" + user.getLogin().getLogin() + "',"
					+ "'" + user.getLogin().getMdp() + "',"
					+  String.valueOf(userTemp.getId()) + ")";
			
			Statement stServices = conn.createStatement();
			stServices.executeUpdate(strQuery);
			
			stServices.close();
			conn.commit();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param user
	 */
	public static void updateLogin(Utilisateur user) {
		if (user.getLogin() != null) {
			try {
				String driverName = "org.postgresql.Driver";
				Class.forName(driverName);
				
				Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/libdata", "libadmin", "libadmin");
				conn.setAutoCommit(false);
				
				String strQuery = "UPDATE connexion SET "
						+ "login = '" + user.getLogin().getLogin() + "',"
						+ "mdp = '" + user.getLogin().getMdp() + "'"
						+ " where iduser = " + String.valueOf(GeneralControl.getUtilisateur().getId());
				
				Statement stServices = conn.createStatement();
				stServices.executeUpdate(strQuery);
				
				stServices.close();
				conn.commit();
				conn.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	
	/**
	 * 
	 * @param user
	 */
	public static void deleteLogin(Utilisateur user) {
		try {
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/libdata", "libadmin", "libadmin");
			conn.setAutoCommit(false);
			
			String strQuery = "DELETE FROM connexion where iduser = " + user.getId();
			
			Statement stServices = conn.createStatement();
			stServices.executeUpdate(strQuery);
			
			stServices.close();
			conn.commit();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	

}
