package fr.afpa.vue;

import java.util.Scanner;

import fr.afpa.control.GeneralControl;

public class Menu {

	/**
	 * Menu de selection d'element � modifier pour l'annonce
	 */
	public static void menuModifAnnonce() {
			String menu = " ----> Que voulez-vous modifier ? <---- \n";
			menu = menu + " 1 -> Titre \n";
			menu = menu + " 2 -> Quantit� \n";
			menu = menu + " 3 -> Prix \n";
			menu = menu + " 4 -> Remise \n";
			menu = menu + " 5 -> Maison d'�dition \n";
			menu = menu + " 6 -> ISBN \n";
			menu = menu + " 7 -> Date d'�dition \n";
			menu = menu + " 8 -> Date d'annonce \n";
			menu = menu + " 9 -> Retour";
			System.out.println(menu);
	}
	
	public static void menuFiltre() {
		String menu = " ----> Quelle filtre voulez vous appliquer ? <---- \n";
		menu = menu + " 1 -> Par Ville \n";
		menu = menu + " 2 -> Par ISBN \n";
		menu = menu + " 3 -> Par Titre \n";
		menu = menu + " 4 -> Retour";
		System.out.println(menu);
}

	
	/**
	 * Menu de selection d'element � modifier pour le client
	 */
	public static void menuModifCompte() {
		String menu = " ----> Que voulez-vous modifier ? <---- \n";
		menu = menu + " 1 -> Nom \n";
		menu = menu + " 2 -> Pr�nom \n";
		menu = menu + " 3 -> Mail \n";
		menu = menu + " 4 -> T�l�phone \n";
		menu = menu + " 5 -> Login \n";
		menu = menu + " 6 -> Mot de Passe \n";
		menu = menu + " 7 -> Retour";
		System.out.println(menu);
}
	
	/**
	 * Menu de manipulation d'annonces
	 */
	public static void menuAnnonce() {
		
		boolean condition = true;
		Scanner in = new Scanner(System.in);
		String choix = "";
		
		do {
			
			String menu = " ----> Menu <---- \n";
			menu = menu + " 1 -> Poster une Annonce \n";
			menu = menu + " 2 -> Modifier une Annonce \n";
			menu = menu + " 3 -> Supprimer une Annonce \n";
			menu = menu + " 4 -> Lister vos Annonces \n";
			menu = menu + " 5 -> Lister toutes les Annonces (Filtres) \n";
			menu = menu + " 6 -> Param�tres du Compte \n";
			menu = menu + " 7 -> Se deconnecter";
			
			System.out.println(menu);
			

			System.out.println("Que voulez-vous ? : ");
			choix = in.nextLine();
			
			if (choix.equals("1")) {
				GeneralControl.posterAnnonce();
			}else if (choix.equals("2")) {
				GeneralControl.modifierAnnonce();
			}else if (choix.equals("3")) {
				GeneralControl.supprimerAnnonce();
			}else if (choix.equals("4")) {
				GeneralControl.listerSelfAnnonces();
			}else if (choix.equals("5")) {
				GeneralControl.listAnnonceFiltre();
			}else if (choix.equals("6")) {
				boolean menuCompte =  menuCompte();
				if (menuCompte) {
					condition = false;
				}
				
			}else if (choix.equals("7")) {
				condition = false;
			}else {
				System.out.println("Entrez une saisie correct");
			}
			
		} while (condition);
		
	}
	
	/**
	 * Param�tres du compte
	 * @return
	 */
	public static boolean menuCompte() {
		
		boolean condition = true;
		Scanner in = new Scanner(System.in);
		String choix = "";
		
		do {
			
			String menu = " ----> Param�tres Compte <---- \n";
			menu = menu + " 1 -> Consulter vos informations \n";
			menu = menu + " 2 -> Modifier vos informations \n";
			menu = menu + " 3 -> Supprimer votre compte \n";
			menu = menu + " 4 -> Retour";
			
			System.out.println(menu);
			

			System.out.println("Que voulez-vous ? : ");
			choix = in.nextLine();
			
			if (choix.equals("1")) {
				GeneralControl.consulterInfos();
			}else if (choix.equals("2")) {
				GeneralControl.modifierCompte();
				condition = false;
				return true;
			}else if (choix.equals("3")) {
				GeneralControl.supprimerCompte();
				condition = false;
				return true;
			}else if (choix.equals("4")) {
				condition = false;
			}else {
				System.out.println("Entrez une saisie correct");
			}

		} while (condition);
		
		return false;
		
	}
	
	
	/**
	 * Menu de login
	 */
	public static void login() {
		
		Scanner in = new Scanner(System.in);
		
			
		System.out.println("Entrez votre Login : ");
		String log = in.nextLine();
		
		System.out.println("Entrez votre Mot De Passe : ");
		String mdp = in.nextLine();
		
		if (GeneralControl.login(log, mdp)) {
			menuAnnonce();
		}else {
			System.out.println("Login ou Mot de Passe incorrect");
		}
		
		
	}


	/**
	 * Accueil
	 */
	public static void accueil() {
		
		boolean condition = true;
		Scanner in = new Scanner(System.in);
		String choix = "";
		
		do {
			
			String menu = " ----> Accueil <---- \n";
			menu = menu + " 1 -> Se connecter \n";
			menu = menu + " 2 -> Cr�er un compte \n";
			menu = menu + " 3 -> Quitter";
			
			System.out.println(menu);
			
	
			System.out.println("Que voulez-vous ? : ");
			choix = in.nextLine();
			
			if (choix.equals("1")) {
				login();
			}else if (choix.equals("2")) {
				GeneralControl.creerCompte();
			}else if (choix.equals("3")) {
				System.out.println("Au revoir !");
				condition = false;
			}else {
				System.out.println("Entrez une saisie correct");
			}
			
		} while (condition);
		
		
		
	}

}
