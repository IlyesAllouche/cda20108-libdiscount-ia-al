package fr.afpa.service;

import java.time.LocalDate;
import java.util.Scanner;

import fr.afpa.beans.Adresse;
import fr.afpa.beans.Annonce;
import fr.afpa.beans.Connexion;
import fr.afpa.beans.Utilisateur;
import fr.afpa.control.GeneralControl;
import fr.afpa.control.SaisieControl;
import fr.afpa.dao.DaoAnnonce;
import fr.afpa.vue.Menu;

public class ServiceUtilisateur {

	public static Utilisateur creerCompte() {
		Scanner in = new Scanner(System.in);

		System.out.println("Entrez votre Nom : ");
		String nom = in.nextLine();

		while (SaisieControl.justStr(nom) == false) {
			System.out.println("Re-Entrez votre Nom : ");
			nom = in.nextLine();
		}

		System.out.println("Entrez votre Prenom : ");
		String prenom = in.nextLine();

		while (SaisieControl.justStr(prenom) == false) {
			System.out.println("Re-Entrez votre Prenom : ");
			prenom = in.nextLine();
		}

		System.out.println("Entrez votre adresse Mail : ");
		String mail = in.nextLine();

		System.out.println("Entrez votre numero de Telephone : ");
		String tel = in.nextLine();

		while (SaisieControl.justNumeric(tel) == false) {
			System.out.println("Entrez votre numero de Telephone : ");
			tel = in.nextLine();
		}

		System.out.println("Entrez le nom de votre Librairie : ");
		String nomLibrairie = in.nextLine();

		System.out.println("Entrez l'adresse de la librairie : ");
		String adresse = in.nextLine();

		System.out.println("Entrez votre ville : ");
		String ville = in.nextLine();

		System.out.println("Entrez votre login : ");
		String login = in.nextLine();

		System.out.println("Entrez votre Mot De Passe : ");
		String mdp = in.nextLine();

		Adresse adr = new Adresse(adresse, ville);

		Connexion log = new Connexion();
		log.setLogin(login);
		log.setMdp(mdp);

		Utilisateur user = new Utilisateur();
		user.setAdresse(adr);
		user.setNom(nom);
		user.setPrenom(prenom);
		user.setTelephone(tel);
		user.setNomLibrairie(nomLibrairie);
		user.setMail(mail);
		user.setLogin(log);

		GeneralControl.setUtilisateur(user);

		return user;
	}

	public static void consulterInfos(Utilisateur utilisateur) {
		System.out.println("Voici vos information : ");
		System.out.println(utilisateur);
	}

	public static Utilisateur modifierCompte(Utilisateur utilisateur) {
		GeneralControl.consulterInfos();

		Scanner in = new Scanner(System.in);

		Utilisateur user = utilisateur;

		boolean condition = true;
		String choix = "";

		do {

			Menu.menuModifCompte();

			System.out.println("Que voulez-vous ? : ");
			choix = in.nextLine();

			if (choix.equals("1")) {

				System.out.println("Entrez le nouveau nom : ");
				String nom = in.nextLine();
				user.setNom(nom);

				return user;

			} else if (choix.equals("2")) {

				System.out.println("Entrez le nouveau pr�nom : ");
				String prenom = in.nextLine();
				user.setPrenom(prenom);

				return user;

			} else if (choix.equals("3")) {

				System.out.println("Entrez le nouveau mail : ");
				String mail = in.nextLine();
				user.setMail(mail);

				return user;

			} else if (choix.equals("4")) {

				System.out.println("Entrez le nouveau num. de telephone : ");
				String tel = in.nextLine();
				user.setTelephone(tel);

				return user;

			} else if (choix.equals("5")) {

				System.out.println("Entrez le nouveau Login : ");
				String login = in.nextLine();
				user.getLogin().setLogin(login);

				return user;

			} else if (choix.equals("6")) {

				System.out.println("Entrez le nouveau Mot de pase : ");
				String mdp = in.nextLine();
				user.getLogin().setMdp(mdp);
				return user;

			} else if (choix.equals("7")) {
				condition = false;
			} else {
				System.out.println("Entrez une saisie correct");
			}

		} while (condition);
		return null;
	}

}
