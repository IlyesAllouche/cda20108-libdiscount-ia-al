

Avant de lancer le .jar :



--> BDD :

- Ouvrez votre interface PGadmin sur votre navigateur

- Créez un superutilisateur avec, pour identifiant : "libadmin", et mot de passe : "libadmin"

- Créez une base de données se nommant : "libdata" et "libadmin" pour OWNER

- Ouvrez votre logiciel client, DBeaver par exemple et connectez-vous à votre base de données

- Ouvrez le script "Script.sql" se trouvant dans le dossier "ScriptSQL"

- Suivez et executez les étapes indiquées une par une

- Faites un click droit sur votre base de données et selectionnez "Régénérer(F5)"

- Verifier que les 4 tables "utilisateur, annonce, connexion, archive" sont présentes

- Verifier que la fonction "archivage()" est présente

- Verifier que les 2 séquences "incrementannnonce, incrementuser" sont présentes

- Verifier que le trigger "archiver_annonce" est présent



--> Une fois toutes ces étapes faites :

- Allez dans le dossier principal du projet, puis dans le dossier "target"

- Copiez-Collez "cda20108-libDiscount-IA-AL.jar" sur votre bureau

- Ouvrez un cmd (invite de commandes) et allez dans le répertoire de votre bureau -> "cd Desktop" ou "cd Bureau" selon votre machine

- Entrez la commande "java -jar cda20108-libDiscount-IA-AL.jar"

- A partir de la vous pouvez créer votre compte et utiliser le reste de l'application